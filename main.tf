terraform {
  backend "s3" {
    bucket = "demo-state"
    key    = "pipeline-demo/state/terraform.tfstate"
    region = "eu-west-2"
  }
}

provider "aws" {
  region = "eu-west-2"
}

data "aws_availability_zones" "current" {}

locals {
  az_count = "${length(data.aws_availability_zones.current.names)}"
}

resource "aws_vpc" "main" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "private" {
  count = "${local.az_count}"

  availability_zone = "${element(data.aws_availability_zones.current.names, count.index)}"
  vpc_id            = "${aws_vpc.main.id}"

  cidr_block = "10.0.${count.index + 1}.0/24"
}

resource "aws_subnet" "public" {
  count = "${local.az_count}"

  availability_zone = "${element(data.aws_availability_zones.current.names, count.index)}"
  vpc_id            = "${aws_vpc.main.id}"

  cidr_block = "10.0.${count.index + local.az_count + 1}.0/24"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_internet_gateway" "main" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_route" "internet" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.main.id}"
}

resource "aws_route_table_association" "private" {
  count = "${local.az_count}"

  route_table_id = "${element(aws_route_table.private.*.id, count.index)}"
  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
}

resource "aws_route_table_association" "public" {
  count = "${local.az_count}"

  route_table_id = "${element(aws_route_table.public.*.id, count.index)}"
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
}

resource "aws_security_group" "lb" {
  name        = "LB Security Group"
  description = "Security group for the application load balancer"
  vpc_id      = "${aws_vpc.main.id}"
}

resource "aws_security_group" "app" {
  name        = "App Security Group"
  description = "Security group for the application instances"
  vpc_id      = "${aws_vpc.main.id}"
}

resource "aws_security_group_rule" "lb_ingress_80" {
  type        = "ingress"
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.lb.id}"
}

resource "aws_security_group_rule" "lb_eggress_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.lb.id}"
}

resource "aws_security_group_rule" "app_ingress_80" {
  type      = "ingress"
  from_port = 80
  to_port   = 80
  protocol  = "tcp"

  source_security_group_id = "${aws_security_group.lb.id}"
  security_group_id        = "${aws_security_group.app.id}"
}

resource "aws_security_group_rule" "app_eggress_all" {
  type        = "egress"
  from_port   = 0
  to_port     = 65535
  protocol    = "all"
  cidr_blocks = ["0.0.0.0/0"]

  security_group_id = "${aws_security_group.app.id}"
}

data "aws_ami" "app" {
  most_recent = true
  owners      = ["777607327140"]

  filter {
    name   = "name"
    values = ["test-ami-*"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_launch_configuration" "app" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix     = "pipeline-demo-conf"
  image_id        = "${data.aws_ami.app.id}"
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.app.id}"]
}

resource "aws_lb_target_group" "app" {
  name     = "pipeline-demo-app"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${aws_vpc.main.id}"
}

resource "aws_autoscaling_group" "app" {
  lifecycle {
    create_before_destroy = true
  }

  name     = "pipeline-demo-asg"
  max_size = 2
  min_size = 2

  launch_configuration = "${aws_launch_configuration.app.id}"
  target_group_arns    = ["${aws_lb_target_group.app.arn}"]

  vpc_zone_identifier = ["${aws_subnet.public.*.id}"]
}

resource "aws_lb" "app" {
  name               = "pipeline-demo-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.lb.id}"]
  subnets            = ["${aws_subnet.public.*.id}"]
}

resource "aws_lb_listener" "app" {
  load_balancer_arn = "${aws_lb.app.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    target_group_arn = "${aws_lb_target_group.app.arn}"
    type             = "forward"
  }
}
